<?php

/**
 * @file Up RPC Users - batch functions
 *
 * The flow of operation is this:
 * (The user module is a good example as users are simpler than nodes!)
 *
 *  - The batch operation begins
 *  - Run a remote query to count the number of items to import
 *    This sets the batch sandbox count and in fact determines whether the batch should run at all.
 *  - Run a remote query to get the ids of the next lot of items to import.
 *    (This is the same as getting a View on Content Distribution, in effect)
 *  - For each id in the returned list:
 *    - Import the item
 *    - Register it as having been imported, both locally and on the remote site.
 */

/**
 * Up RPC batch operation
 */
function _giuser_batch_users(&$context) {
  $limit = 10;

  module_load_include("inc", "uprpc");

  // This must be a ' ' at the least to deal with a WTF in Services.
  // @see uprpc_services_get_import_list() for more detail.
  $joins = ' '; // todo!

  if (!$context['sandbox']) {
    // Don't process Anonymous or Admin (0 and 1 respectively)
    $max = _uprpc_xmlrpc('query.count_unimported', 'users', 'uid', $joins, 'uid > 1');

    if (!$max) {
      drupal_set_message("No users to process: have we already run the import?");
      $context['finished'] = 1;
      return;
    }

    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = $max;
  }

  // Process $limit users
  // Get the next lot of ids from the remote site as a flat array.
  $ids = _uprpc_xmlrpc('query.get_import_list', 'users', 'uid', $limit, $joins, 'uid > 1');
  foreach ($ids as $uid) {
    // Needs casting to an int to get sent back via XMLRPC, for fuck knows what reason.
    $uid = (int) $uid;

    $new = giuser_import_user($uid);
    uprpc_register_ids((int) $uid, $new->uid, "user");
    // Mark the remote item as having been imported.
    if (variable_get('uprpc_remote_mark', TRUE)) {
      _uprpc_xmlrpc('query.set_imported', 'users', $uid);
    }
    $context['sandbox']['progress']++;
  }

  // Batch API progress
  // Trap if we go over the maximum number - set to maximum!
  if ($context['sandbox']['progress'] > $context['sandbox']['max']) {
    $context['sandbox']['progress'] = $context['sandbox']['max'];
  }
  $context['message'] = $context['sandbox']['progress'] ." users processed out of " . $context['sandbox']['max'];
  $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
}

/**
 * Import a single user
 *
 * @param $remote_uid
 *  The uid of a user on the remote site.
 */
function giuser_import_user($remote_uid) {
  // Get the existing local user (if one exists) for the remote ID.
  // This may get us FALSE.
  $local_user = uprpc_retrieve_by_old_id('user', $remote_uid);

  $remote_user = _giuser_rpc_user($remote_uid);
  if (!$remote_user) {
    watchdog("uprpc", "Cannot find user old=@old", array("@old" => $remote_uid), WATCHDOG_WARNING);
    return;
  }

  $fields = user_fields();
  $edit = array();
  foreach ($fields as $f) {
    switch($f) {
      case 'uid':
      case 'pass':
      break;

      case 'signature_format':
      case 'timezone_name':
        // Let D6 put in defaults for this
      break;

      default:
        $edit[$f] = $remote_user[$f];
    }
  }
  
  // Invoke hook_userimport_user_preprocess().
  // There's no notion of a handling module, so we invoke in all, though
  // in theory you probably just want your one configuration module to
  // implement this.
  // This allows you to do things like map the roles.
  foreach (module_implements('userimport_user_preprocess') as $module) {
    // Don't use module_invoke() so both can be passed as references.
    $function = $module . '_userimport_user_preprocess';
    $function($remote_user, $local_user, $edit);
  }

  // If there is no local user -- ie, the remote user to import is completely new to
  // this database, try to give it the same uid.
  if (!$local_user) {
    db_query("ALTER TABLE {users} AUTO_INCREMENT = %d", $remote_uid); 
  }

  $local_user = user_save($local_user, $edit);

  if (!$local_user->uid) {
    watchdog("uprpc", "Cannot import user old=@old", array("@old" => $remote_uid), WATCHDOG_WARNING);
  }

  // Password would have been doubly md5'ed, so set explicitly
  db_query("UPDATE {users} SET pass = '%s' WHERE uid = %d", $remote_user['pass'], $local_user->uid);

  return $local_user;
}

/**
 * Retrieve user by ID.
 *
 * @param $remote_uid
 *  The uid of a user on the remote site.
 */
function _giuser_rpc_user($remote_uid) {
  return _uprpc_xmlrpc('user.load', $remote_uid);
}

/**
 * Up RPC batch operation
 */
function _giuser_batch_node_owners(&$context) {
  $limit = 10;

  module_load_include("inc", "uprpc");

  if(!$context['sandbox']) {
    // Don't process Anonymous or Admin (0 and 1 respectively)
    $num_q = db_fetch_array(db_query("SELECT COUNT(nid) AS num FROM {import_phases} WHERE phase & %d = 0", UPRPC_PHASE_UIDS));
    $max = $num_q['num'];
    if (!$max) {
      drupal_set_message("No node owners to process: have we already run the import?");
      $context['finished'] = 1;
      return;
    }

    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = $max;
  }

  // Process $limit users
  $result = db_query("SELECT ip.nid, n.uid FROM {import_phases} ip INNER JOIN {node} n ON n.nid = ip.nid WHERE ip.phase & %d = 0 ORDER BY ip.nid LIMIT $limit", UPRPC_PHASE_UIDS);
  while($row = db_fetch_array($result)) {
    // Get new user
    $uid = uprpc_retrieve_by_old_id("user", $row['uid'], TRUE);
    if($uid) {
      // Overwrite uid on node and node revisions
      uprpc_force_uid_for_nid($row['nid'], $uid);
    }
    // Set import phase for this one
    uprpc_set_phase($row['nid'], UPRPC_PHASE_UIDS);
    $context['sandbox']['progress']++;
  }

  // Batch API progress
  // Trap if we go over the maximum number - set to maximum!
  if ($context['sandbox']['progress'] > $context['sandbox']['max']) {
    $context['sandbox']['progress'] = $context['sandbox']['max'];
  }
  $context['message'] = $context['sandbox']['progress'] ." nodes processed out of " . $context['sandbox']['max'];
  $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
}

