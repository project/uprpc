<?php
/**
 * @file uprpc_import_node_filefield.module
 * Profides a filefield handler.
 */

/**
 * Implementation of hook_init().
 */
function uprpc_import_node_filefield_init() {

}

/**
 * Implementation of hook_nodeimport_field_handlers.
 *
 * Return data about field handlers.
 */
function uprpc_import_node_filefield_nodeimport_field_handlers() {
  return array(
    'file' => 'uprpc_import_node_filefield_process',  
  );  
}

/**
 * Handler for CCK filefield.
 *
 * @param $old
 *  The old node, as an array.
 * @param $new
 *  The new node object.
 * @param $field_mapping
 *  The field mapping for the field in question, as given by hook_nodeimport_fields().
 */
function uprpc_import_node_filefield_process(&$old, &$new, $field_mapping) {
  $old_key = $field_mapping['from'];
  $new_key = $field_mapping['to'];

  //dsm($field);
  //dsm($old[$old_key]);
  
  // Get CCK's data about the local node type and thence a list of its fields.
  $content_type_info = content_types($new->type);
	$fields = $content_type_info['fields'];
  $field = $fields[$new_key];
  //dsm($field);
  
  // Get filepath to save to, using the settings from the CCK widget.
  // filefield module's filefield_widget_file_path() gets us a complete
  // path to the right directory.
  $destination = filefield_widget_file_path($field);
  if (!field_file_check_directory($destination, FILE_CREATE_DIRECTORY)) {
    watchdog('filefield', 'The upload directory %directory for the file field %field (content type %type) could not be created or is not accessible. A newly uploaded file could not be saved in this directory as a consequence, and the upload was canceled.', array(
    '%directory' => $destination, 
    '%field' => $field['field_name'], 
    '%type' => $node_type,
  ));
    return;
  }
  
  
  $remote_url = variable_get('uprpc_server', '');
  
  // At this point we could be clever and use Services to get the file.
  // However, there's no file service on D5, so we'd need to backport the
  // module at http://drupal.org/node/290053 and bundle it in.
  // This is fine if all the files are public, but this needs fixing if they are not.
  if (is_array($old[$old_key])) {
    foreach ($old[$old_key] as $delta => $data) {
      $stuff = uprpc_import_node_filefield_process_file($old, $new, $field_mapping, $field, $remote_url, $delta, $data);
    }
  }
}

/**
 * Handle a single file.
 *
 * Yes, it's a ton of variables. Try not to think about it too hard.
 *
 * @param $old
 *  The old node, as an array.
 * @param $new
 *  The new node object.
 * @param $field_mapping
 *  The field mapping for the field in question, as given by hook_nodeimport_fields().
 * @param $field
 *  The CCK definition for the current filefield.
 * @param $remote_url
 *  The base URL of the remote site.
 * @param $delta
 *  The delta of the current file on the old node.
 * @param $file
 *  The data for the current file on the old node.
 */
function uprpc_import_node_filefield_process_file($old, $new, $field_mapping, $field, $remote_url, $delta, $file) {
  // We have no timestamps coming in from D5.
  // So pretty much have to bring the file across regardless.
  
  $file_url = $remote_url . '/' . $file['filepath'];
  
  // The URL may have spaces in and PHP is too stupid to figure this out.
  $file_url = str_replace(' ', '%20', $file_url);
  
  $file_data = file_get_contents($file_url);
  if ($file_data === FALSE) {
    // an error happened
  }
  
  // Create the filepath to save to.
  $file['filepath'] = filefield_widget_file_path($field) . '/' . $file['filename'];
  
  //dsm($file);
  //dsm($field_mapping);
  //dsm($new);
  
  // In all cases, remove the fid that came over with the old node.
  unset($file['fid']);
  
  // Convert title and alt data to D6 format.
  $file['data'] = array(
    'title' => $file['title'],
    'alt'   => $file['alt'],
  );
  $file['list'] = 1;
  
  // Three cases, fairly complicated to follow....
  if (isset($new->{$field_mapping['to']})) {
    //dsm($new->{$field_mapping['to']}[$delta]);
    // There is an existing file on the node:
    // either it's our file or a different file.

    // In either case, delete the physical file, references be damned!
    file_delete($new->{$field_mapping['to']}[$delta]['filepath']);

    // Save the incoming file.
    // Gah this is so bloody convoluted. Roll on D7.
    $filepath = file_save_data($file_data, $file['filepath'], FILE_EXISTS_RENAME);
    $file['filepath'] = $filepath;
    
    if ($new->{$field_mapping['to']}[$delta]['filename'] == $file['filename']) {
      // Case 1: the file exists already on the new node.
      // We want to keep the file record, just update it.
      //dsm('case 1');
      
      // Set the fid on the incoming data as we're about to save this.
      $file['fid'] = $new->{$field_mapping['to']}[$delta]['fid'];
      
      // Update the DB record, keeping the fid.
      drupal_write_record('files', $file, 'fid');
    }
    else {
      // Case 2: node has a different file we should delete.
      //dsm('case 2');

      // Delete the existing file record.
      $old_fid = $new->{$field_mapping['to']}[$delta]['fid'];
      db_query('DELETE FROM {files} WHERE fid = %d', $old_fid);

      // Write record in files table.
      drupal_write_record('files', $file);
    }
  }
  else {
    // Case 3: no file on the new node. This is a new incoming file.
    //dsm('case 3');

    
    $filepath = file_save_data($file_data, $file['filepath'], FILE_EXISTS_RENAME);
    $file['filepath'] = $filepath;

    // Write record in files table.
    drupal_write_record('files', $file);
  }

  // Put the new file record into the node
  $new->{$field_mapping['to']}[$delta] = $file;

}


