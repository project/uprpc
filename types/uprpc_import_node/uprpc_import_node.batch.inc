<?php

/**
 * @file
 * Contains batch functions for importing nodes.
 */

/**
 * Batch callback for processing nodes
 *
 * @todo most of this function could do to be abstracted.
 * see http://drupal.org/project/field_convert for example.
 */
function _uprpc_batch_nodes(&$context) {
  $limit = 10;

  module_load_include("inc", "uprpc");

  // Get the content types as saved on the admin settings form.
  $content_types = array_filter(variable_get('uprpc_types', ''));

  // Node import works a little differently from users and terms.
  // Some content types should not ALL be migrated.
  // We invoke hook_nodeimport_query() (see that for note about reworking needed!)
  // to give us extra JOIN and WHERE clauses for the queries, and thus
  // we alter the count and list queries to send to the remote site for each type.
  // However, this means we get the WHOLE list of nids at the start of the batch
  // and store it as a big fat array of nids in the batch sandbox.
  // I am reliable told on IRC that an array of some 8000 nodes ids should
  // be manageable.
  // The altermative would be to muck around either creating one batch operation
  // for each node type, or being quite creative in getting the next
  // set of 10 nids to work on, and probably storing an array of types,
  // working on nids from the first one, popping it off the array once we 
  // have no more, etc etc.

  // Invoke hook_nodeimport_query() to see if there are any changes to be made to the queries for these types.
  // @see the query built in uprpc_services_get_import_list().
  $query_data = module_invoke_all('nodeimport_query');
  foreach ($content_types as $type) {
    $joins[$type] = ' '; // always have to send at least a space; Services chokes on empty string.
    $where[$type] = " b.type = '$type' ";
    if (isset($query_data[$type])) {
      $joins[$type] .= $query_data[$type]['joins'];
      $where[$type] .= ' AND ' . $query_data[$type]['where'];
    }
  }

  if (!$context['sandbox']) {
    // Get the total number of nodes to import.
    foreach ($content_types as $type) {
      $max_type = _uprpc_xmlrpc('query.count_unimported', 'node', 'nid', $joins[$type], $where[$type]);
      $max += $max_type;
    }
    
    if (!$max) {
      drupal_set_message("No nodes to process: have we already run the import?");
      $context['finished'] = 1;
      return;
    }

    // Build a list of all the node ids we need to process.
    $list = array();
    foreach ($content_types as $type) {
      $list_type = _uprpc_xmlrpc('query.get_import_list', 'node', 'nid', 0, $joins[$type], $where[$type]);
      $list = array_merge($list, $list_type);
    }

    $context['sandbox']['ids'] = $list;
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = $max;
  }

  // Get more nids to process.
  $nids = array_splice($context['sandbox']['ids'], 0, 10);

  module_load_include("inc", "uprpc");
  foreach ($nids as $nid) {
    // Needs casting to an int to get sent back via XMLRPC, for fuck knows what reason.
    $nid = (int) $nid;
    $new_node = uprpc_import_node($nid);

    // Mark the remote item as having been imported.
    // FIXME! @TODO: only do this if the import actually got us something!?!?!
    // one of the cases that breaks is when there is no existing content type for the node.
    if (!is_null($new_node) && variable_get('uprpc_remote_mark', TRUE)) {
      _uprpc_xmlrpc('query.set_imported', 'node', (int) $nid);
    }
    
    $context['sandbox']['progress']++;
  }

  // Batch API progress
  // Trap if we go over the maximum number - set to maximum!
  if ($context['sandbox']['progress'] > $context['sandbox']['max']) {
    $context['sandbox']['progress'] = $context['sandbox']['max'];
  }
  $context['message'] = $context['sandbox']['progress'] ." nodes processed out of " . $context['sandbox']['max'];
  $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
}

/**
 * Batch callback for processing nid fixes
 */
function uprpc_batch_nid_fixes(&$context) {
  $limit = 10;

  module_load_include("inc", "uprpc");

  if(!$context['sandbox']) {
    $num_q = db_fetch_array(db_query("SELECT COUNT(nid) AS num FROM {import_phases} WHERE phase & %d = 0", UPRPC_PHASE_NIDS));
    
    $max = $num_q['num'];
    if (!$max) {
      drupal_set_message("No nodes to process: have we already run the nid fix");
      $context['finished'] = 1;
      return;
    }

    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = $max;
  }

  // Process $limit nodes
  $result = db_query("SELECT nid FROM {import_phases} WHERE phase & %d = 0 ORDER BY nid LIMIT $limit", UPRPC_PHASE_NIDS);
  while($row = db_fetch_array($result)) {
    uprpc_import_fix_nids(node_load($row['nid']));
    $context['sandbox']['progress']++;
    uprpc_set_phase($row['nid'], UPRPC_PHASE_NIDS);
  }

  // Batch API progress
  // Trap if we go over the maximum number - set to maximum!
  if ($context['sandbox']['progress'] > $context['sandbox']['max']) {
    $context['sandbox']['progress'] = $context['sandbox']['max'];
  }
  $context['message'] = $context['sandbox']['progress'] ." nid fixes out of " . $context['sandbox']['max'];
  $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
}

/**
 * Import a node
 *
 * TODO: we have here 3 almost identical functions for user, term node.
 * these should be refactored into a single function that does the chugging
 * based on some settings or helper functions, which then should be farmed out to submodules.
 *
 * @param $remote_nid
 *  The nid of a node on the remote site.
 */
function uprpc_import_node($remote_nid) {
  // Get the existing local node (if one exists) for the remote ID.
  // This may get us FALSE.
  $local_node = uprpc_retrieve_by_old_id('node', $remote_nid);
  $old_node = _uprpc_rpc_node($remote_nid);
  
  //dsm($remote_nid);
  //dsm($local_node);
  //dsm($old_node);

  if (!$old_node['type']) {
    return;
  }

  // Get configuration from all supporting modules.
  $config = module_invoke_all('nodeimport', 'types');
  if (isset($config[$old_node['type']])) {
    $handler = $config[$old_node['type']];

    // Try to load function and then run
    module_load_include("inc", $module, "$module.nodeimport");
    if (function_exists($handler)) {
      $handler($old_node, $local_node);
      // Register ID and flag node as new, to have all further phases run on it
      uprpc_register_ids($old_node['nid'], $local_node->nid, 'node');
      uprpc_set_phase($local_node->nid);
      return $local_node;
    }
  }

  // @todo: fallback to uprpc_nodeimport_standard() if no handler found
  // instead of just bailing.
  drupal_set_message(t("I couldn't handle the node !nid of type '!type', so I had to quit early. Sorry.",
    array("!nid" => $remote_nid, "!type" => $old_node['type'])));
  exit();
}

/**
 * Fix nid references on a node
 */
function uprpc_import_fix_nids($n) {
  $changed = FALSE;

  // All supporting modules
  // @todo: refactor this as in uprpc_import_node() above.
  foreach (module_implements('nodeimport') as $module) {
    $function = $module . "_nodeimport";
    $config = $function('types');
    $handler = $config[$n->type];
    if (!$handler) { continue; } // todo: code style!!

    module_load_include("inc", $module, "$module.nodeimport");
    $cck_fn = $module . "_nodeimport_fields";
    if (!function_exists($cck_fn)) { continue; }

    // Process node references
    foreach($cck_fn() as $conf) {
      if($conf['type'] != "nodereference") { continue; }

      $f = $conf['to'];
      foreach($n->{$f} as $i => $old_data) {
        if ($old_data['nid']) {
          $new_nid = uprpc_retrieve_by_old_id("node", $old_data["nid"], TRUE);
          if ($new_nid) {
            $n->{$f}[$i]["nid"] = $new_nid;
            $changed = TRUE;
          }
        }
      }
    }
  }
  // todo: URGH!
  $changed && uprpc_node_save($n);
}


/**
 * Standard import procedure across most nodes
 *
 * @param $old
 *  The old node, as an array.
 * @param $new
 *  The new node (if one exists yet; may be NULL).
 * @param $module
 *  The base handler module for this node type.
 *  Note that currently this is supplied direct by the caller, rather than
 *  passed from the original invocation.
 */
function uprpc_nodeimport_standard(&$old, &$new, $module = NULL) {
  // Invoke hook_nodeimport_node_preprocess() in the calling module.
  // This allows you to do things like change the type of the incoming node
  // before core mapping saves it.
  if (module_hook($module, 'nodeimport_node_preprocess')) {
    $function = $module . '_nodeimport_node_preprocess';
    $function($old, $new);
  }

  // Do some basic out-of-box mapping
  uprpc_nodeimport_core($old, $new);
  
  // Now we've done the basics, let other modules process the node.
  foreach (module_implements('nodeimport_node_process') as $hook_module) {
    // Don't use module_invoke() so we can pass by reference.
    $function = $hook_module . '_nodeimport_node_process';
    $function($old, $new, $module);
  }

  // Non-ASCII characters?
  if ($bad = uprpc_nodeimport_has_bad_codepoints($new)) {
    watchdog("uprpc", "Encoding fix on node old=@old failed for character @chr; node saved anyhow with new=@new",
      array('@chr' => $bad, '@old' => $old['nid'], '@new' => $new->nid), WATCHDOG_ERROR);
  }

  // Final save
  uprpc_node_save($new);
}

/**
 * Basic processing for an imported node.
 *
 * Create a new node of type $type, and save so we have a nid.
 */
function uprpc_nodeimport_core(&$old, &$new, $type = NULL, $save = TRUE) {
  // Ensure we have a functioning new node object to import into
  $new || ($new = array('uid' => 0, 'name' => '', 'language' => ''));
  is_object($new) || ($new = (object)$new);

  //dsm($old);
  //dsm($new);

  // Check whether old node has bad encoding
  $old['_import_bad_codepoint'] = uprpc_nodeimport_has_bad_codepoints($old);

  // 1. Overwrite
  $new->type = $type ? $type : $old['type'];

  // 2a. Codepoint filter
  foreach (array('title', 'body') as $key) {
    $old['_import_bad_codepoint'] && ($old[$key] = _uprpc_nodeimport_fix_codepoints($old[$key], $new->nid));
  }

  // 2b. Copy
  foreach (array('title', 'status', 'created', 'comment', 'promote', 'sticky', 'path', 'uid', 'body') as $key) {
    $new->{$key} = $old[$key];
  }

  // TODO
  // comments?
  // formats?
  
  // Fields we DON'T care about but which might cause encoding issues
  unset($old['body_value']);
  unset($old['teaser']);

  // Save it so we definitely get an ID for this node, regardless of what happens later
  uprpc_node_save($new);

  uprpc_set_phase($new->nid);

}

/**
 * Implement hook_nodeimport_node_process() on behalf of CCK content module.
 *
 * @todo: farm this out to a helper module?
 *
 * CCK mapping into an existing new node, based on field mapping definitions
 * for this type.
 *
 * @param $old
 *  The old node, as an array.
 * @param $new
 *  The new node object.
 * @param $module
 *  The base handler module for this node type.
 */
function content_nodeimport_node_process(&$old, &$new, $module) {
  // We can do nothing without a base handler module to give us field mappings.
  if (!$module) {
    return;
  }

  static $field_handlers;
  if (!isset($field_handlers)) {
    $field_handlers = module_invoke_all('nodeimport_field_handlers');
  }
  
  // Get the field mappings for this type.
  $fields = module_invoke($module, 'nodeimport_fields', $old['type']);

  foreach ($fields as $field) {
    // Don't have to specify "to" if they're the same
    if (!isset($field['to'])) {
      $field['to'] = $field['from'];
    }
    $old_key = $field['from'];
    $new_key = $field['to'];
    
    // If there's a registered handler for this field type, use that.
    if (isset($field_handlers[$field['type']])) {
      $field_handler = $field_handlers[$field['type']];
      $field_handler($old, $new, $field);
    }
    else {
      // Otherwise, some simple types are simple ;)
      switch ($field['type']) {
        // Location - not supported yet
        case 'location':
          // TODO
          break;
        // Text - might need to check and filter out bad codepoints
        case 'text':
          if (is_array($old[$old_key])) {
            foreach ($old[$old_key] as $i => $cck) {
              // WTF??? Who the fuck writes this shit? What does it even do?
              $old['_import_bad_codepoint'] && ($old[$old_key][$i]["value"] = _uprpc_nodeimport_fix_codepoints($cck["value"], $new->nid));
            }
          }
        // Link - despite the complex substructure, just copy across
        case 'link':
        // Integer - just copy across 
        case 'integer':
        // Nodereference - we just have to insert the old one and hope they'll get tidied up later
        case 'nodereference':
        // Date: despite the complex substructure, just copy across.
        case 'date':
        default:
          $new->{$new_key} = $old[$old_key];
          break;
      }
    }
  }
}

/**
 * Implement hook_nodeimport_node_process() on behalf of taxonomy module.
 *
 * @todo: farm this out to a helper module?
 *
 * Map taxonomy to imported terms.
 *
 * @param $old
 *  The old node, as an array.
 * @param $new
 *  The new node object.
 * @param $module
 *  The base handler module for this node type.
 */
function taxonomy_nodeimport_node_process(&$old, &$new) {
  foreach ($old['taxonomy'] as $old_term) {
    if ($old_term['tid']) {
      $term = (object) uprpc_retrieve_by_old_id("term", $old_term['tid']);
      if ($term->tid) {
        $new->taxonomy[$term->tid] = $term;
      }
      else {
        dsm("can't find taxonomy for old tid " . $old_term['tid']);
      }
    }
    else {
      dsm("Could not import taxonomy from node " . $old['nid'].": bad record format");
      dsm(var_export($old_term, TRUE));
      exit;
    }
  }
}

/**
 * Try to fix bad codepoints
 *
 * We could try to work out why Services module is serving up non-Unicode
 * text: this might not help as the encoding in the original db itself could be
 * screwy. 
 * Instead, let's replace bad codepoints with Unicode entities.
 *
 * Pass in the node ID so that we can register this import phase was run on this node
 * for checking purposes
 */
function _uprpc_nodeimport_fix_codepoints($text, $nid = 0) {
  // TEMPORARY
  // Most sites won't need unicode fixing! Skip it!
  return $text;
  
  $new_text = preg_replace('/([^\x00-\x80])/e', '"&#".ord(\'$1\').";"', $text, -1, $num);
  // Replaced anything?
  $num && $nid && uprpc_set_phase($nid, UPRPC_PHASE_CODEPOINTS);

  return $new_text;
}

/**
 * Check for bad codepoints; return first one
 */
function uprpc_nodeimport_has_bad_codepoints($node) {
  $dump = var_export($node, TRUE);
  if (preg_match('/[^\x00-\x80]/', $dump, $matches)) {
    return ord($matches[0]);
  }  
}
