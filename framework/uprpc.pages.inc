<?php

/**
 * @file uprpc.pages.inc
 * Contains the form for the import page and its submit handler.
 */

/**
 * Implementation of hook_uprpc_batch_operation_info().
 *
 * Define our base import operations.
 *
 * TODO: add spec for this in an api.php file.
 *
 * For now, in haste: this defines operations available in the batch selection.
 * 
 * @return
 *  An array of operations, keyed by callback.
 *  The callback is assumed to be in a file called MODULENAME.batch.inc -- 
 *  TODO -- add a 'file' key to each op.
 *  The array should contain:
 *    - 'title': the label of the operation.
 *    - 'weight'.
 *  TODO: impose dependencies?
 */
function uprpc_uprpc_batch_operation_info($op = 'batch_ops') {
  return array(
    'uprpc_batch_terms' => array(
      'title'    => t("Import terms"),
      'weight'  => -10, // should come before nodes.
    ),
  );
}

/**
 * Form API callback
 */
function uprpc_page_go($form_values) {
  module_load_include("inc", "uprpc");

  // Now check memory limit, execution time
  $max_execution_time = ini_get('max_execution_time');
  if ($max_execution_time < 600) {
    $warning['time'] = array('#value' => t("Max execution time is !time s. This is too short: consider at least 600s.", array("!time" => $max_execution_time)));
  }
  if ($warning) {
    return $warning;
  }

  // Operations
  $form['operations'] = array(
    '#type' => 'fieldset',
    '#title' => t('Operations'),
    '#description' => t('Select one or more import operations.'),
    '#tree' => TRUE,
  );
  
  // Invoke hook_uprpc_batch_operation_info() to get all import operations 
  // defined by both ourselves and other modules.
  $operations = module_invoke_all('uprpc_batch_operation_info');
  // Sort them by their weight.
  uasort($operations, '_user_sort');

  foreach ($operations as $operation_id => $data) {
    $form['operations'][$operation_id] = array(
      '#type' => 'checkbox',
      '#title' => $data['title'],
    );
  }

  // TODO: move this to a separate form; it makes no sense with the 'import' button.
  $form['reset'] = array(
    '#type' => 'checkbox',
    '#title' => 'Reset all <strong>NODE</strong> import-tracking metrics and start again',
  );

  $form['import'] = array(
    '#type' => 'submit',
    '#value' => 'Import',
  );
  return $form;
}

/**
 * Form submit callback
 */
function uprpc_page_go_submit(&$form, &$form_state) {
  if ($form_state['values']['reset']) {
    module_load_include("inc", "uprpc");
    uprpc_reset_import();
  }

  // Get a flat array of the checked operations.
  $operations = array_keys(array_filter($form_state['values']['operations']));

  module_load_include("inc", "uprpc", "uprpc.batch");
  uprpc_batch_go($operations);
}

/**
 *
 */
function uprpc_manual_page($form_state) {
  $form = array();

  $form['notice'] = array(
    '#type' => 'markup', 
    '#value' => '<p>' . t('Use this page to test your connection is set up correctly.') . '</p>',
    '#weight' => -10,  
  );
  
  // Buttons. These are themed into a vertical list with the description text alongside each one.
  $form['buttons'] = array(
    '#theme' => 'uprpc_manual_form_button',
    '#tree' => TRUE,
  );
  // TODO: all this should be moved a level up to Clients module, which needs a ton of 
  // work to actually support this.
  $form['buttons']['connect'] = array(
    '#value' => 'Test connection',
    '#type' => 'submit',
    '#name' => 'connect', // wtf does this do?
    '#submit' => array('uprpc_manual_connection_submit'),
    '#description' => t('Test the connection settings by calling system.connect on the remote server.'),
  );
  $form['buttons']['login'] = array(
    '#value' => 'Test user login',
    '#type' => 'submit',
    '#name' => 'login',
    '#submit' => array('uprpc_manual_login_submit'),
    '#description' => t('Test the remote user settings and by calling user.login on the remote server.'),
  );
  
  // Allow other modules to add their own manual test buttons.
  // TODO: this hook's implementations could do to be farmed out to MODULE.uprpc.inc files.
  foreach (module_implements('uprpc_manual_check') as $module) {
    // *sigh* going round the houses because module_invoke_all() won't pass by ref.
    $function = $module . '_uprpc_manual_check';
    $function($form);
  }

  // Display the results from the buttons in a fieldset above them.
  // Add this after the buttons but above them so we can grab their human-readable
  // titles from the form array already built.
  if (count($form_state['storage'])) {
    $form['results'] = array(
      '#type' => 'fieldset', 
      '#title' => t('Results'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#weight' => -5,  
    );
    
    foreach (array_keys($form['buttons']) as $button_id) {
      // If there is a key in the form storage with the same ID as a button, 
      // the it is the result of a previous manual test.  
      if (isset($form_state['storage'][$button_id])) {
        $title = $form['buttons'][$button_id]['#value'];
        $form['results'][$button_id] = array(
          '#type' => 'markup', 
          '#value' => "<h3>$title</h3>" . 
            '<pre>' . print_r($form_state['storage'][$button_id], TRUE) . '</pre>',
        );
      }
    }
    
    // Some operations have more results.
    if (isset($form_state['storage']['view_arguments'])) {
      $form['results']['view_arguments'] = array(
        '#type' => 'markup', 
        '#value' => "<h3>View arguments</h3>" . 
          '<pre>' . print_r($form_state['storage']['view_arguments'], TRUE) . '</pre>',
      );
    }
  }
  
  // Empty the form storage so the next test has clean results.
  $form_state['storage'] = array();
  

  return $form;  
}

/**
 * Theme the manual connection form buttons.
 */
function theme_uprpc_manual_form_button($form) {
  foreach (element_children($form) as $e) {
    $button = drupal_render($form[$e]);
    $output .= '<div>' . $button . '<span>' . $form[$e]['#description'] . '</span></div>';
  }
  return $output;
}

/**
 * Submit handler for the connect button.
 */
function uprpc_manual_connection_submit($form, &$form_state) {
  module_load_include("inc", "uprpc");
  $connect = _uprpc_xmlrpc('system.connect');
 
  if (is_array($connect) && isset($connect['user'])) {
    $form_state['storage']['connect'] = $connect;

    drupal_set_message(t('Sucessfully connected to the remote site.'));
  }
  else {
    drupal_set_message(t('Could not connect to the remote site.'), 'warning');
  }
}

/**
 * Submit handler for the login button.
 */
function uprpc_manual_login_submit($form, &$form_state) {
  module_load_include("inc", "uprpc");
  $login = _uprpc_xmlrpc('user.login');
  
  if (is_array($login) && isset($login['user'])) {
    $form_state['storage']['login'] = $login;
    
    drupal_set_message(t('Sucessfully logged in to the remote site; got back details for user %user (uid @uid).', array(
      '%user' => $login['user']['name'],
      '@uid'  => $login['user']['uid'],
      )));
  }
  else {
    drupal_set_message(t('Could not log in to the remote site.'), 'warning');
  }
}
