<?php
/**
 * @file uprpc.module
 * Contains the main module code.
 */

// Phases
define("UPRPC_PHASE_NEW", 0);
// Node IDs in nodereference field replaced?
define("UPRPC_PHASE_NIDS", 1);
// Unicode fixes?
define("UPRPC_PHASE_CODEPOINTS", 2);
// User IDs in node->uid fixed?
define("UPRPC_PHASE_UIDS", 4);

/**
 * Implementation of hook_menu
 */
function uprpc_menu() {
  $items['admin/import'] = array(
    'title' => 'Import',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uprpc_page_go'),
    'access arguments' => array('administer site configuration'),
    'file' => 'uprpc.pages.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  // Dummy menu item to start 2nd level tabs.
  $items['admin/import/import'] = array(
    'title' => 'Import',
    'weight' => -1,
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );


  $items['admin/import/settings'] = array(
    'title' => 'Settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uprpc_settings'),
    'access arguments' => array('administer site configuration'),
    'file' => 'uprpc.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );

  $items['admin/import/manual'] = array(
    'title' => 'Manual tests',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uprpc_manual_page'),
    'access arguments' => array('administer site configuration'),
    'file' => 'uprpc.pages.inc',
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Implementation of hook_perm().
 */
function uprpc_perm() {
  return array('access links to imported originals');
}

/**
 * Helper function to get remote ID of a local object.
 *
 * @param $type
 *  The type, eg 'node', 'user'.
 * @param $local_id
 *  The local id of the object, ie the nid, uid, as relevant.
 */
function uprpc_retrieve_remote_id($type, $local_id) {
  $result = db_query("SELECT old_id  FROM {import_ids} WHERE new_id = %d AND type = '%s'", $local_id, $type);
  $remote_id = db_result($result);
  
  return $remote_id;
}

/**
 * Implementation of hook_link().
 *
 * Show a link to the remote original node.
 */
function uprpc_link($type, $object, $teaser = FALSE) {
  if ($type == 'node' && user_access('access links to imported originals')) {
    $base = variable_get('uprpc_server', '');
    if ($base) {
      $remote_id = uprpc_retrieve_remote_id('node', $object->nid);
      
      if ($remote_id) {
        $links = array();
        $links['uprpc_original'] = array( // a regular link
          'title' => t('Original node on remote site'),
          'href' => $base . '/node/' . $remote_id,
          //'attributes' => array('title' => t('TODO: Fill in link title attribute.')),  
        );
        return $links;
      }
    }
  }
}

/**
 * Implementation of hook_user().
 */
function uprpc_user($op, &$edit, &$account, $category = NULL) {
  if ($op == 'view') {
    $base = variable_get('uprpc_server', '');
    if ($base) {
      $remote_id = uprpc_retrieve_remote_id('user', $account->uid);

      if ($remote_id) {
        $account->content['summary']['imported'] = array(
          '#type' => 'user_profile_category',
          '#title' => 'Imported',
          'imported' => array(
            '#type' => 'user_profile_item',
            '#title' => 'Imported',
            '#value' => l('Original user on remote site', $base . '/user/' . $remote_id),
          ),
        );
      }
    }
  }
}

/**
 * Implementation of hook_theme().
 */
function uprpc_theme($existing, $type, $theme, $path) {
  return array(
    'uprpc_manual_form_button' => array(
      'arguments' => array('form' => array()),
    ),
  );
}
