<?php

/**
 * @file uprpc.inc
 * Import library
 */
// Todo: farm out all the node-related stuff to a node import module, same as the user one. 

/**
 * Retrieve node by ID
 */
function _uprpc_rpc_node($nid, $fields = array()) {
  return _uprpc_xmlrpc('node.load', $nid, $fields);
}

/**
 * Retrieve taxonomy tree by vocabulary ID
 */
function _uprpc_rpc_vocab($vid) {
  return _uprpc_xmlrpc('taxonomy.getTree', (int) $vid);
}

/**
 * Make an XMLRPC call to the remote site.
 *
 * @param $method
 *  The name of the remote method to call.
 * @param
 *  All other parameters are passed to the remote method.
 *  Note that the D5 version of Services does not seem to respect optional parameters; you
 *  should pass in defaults instead of omitting a parameter.
 *
 * @return
 *  Whatever is returned from the remote site. 
 */
function _uprpc_xmlrpc($method) {
  // If HTTP requests are enabled, report the error and do nothing.
  if (variable_get('drupal_http_request_fails', FALSE) == TRUE) {
    drupal_set_message(t('Drupal is unable to make HTTP requests. Please reset the HTTP request status by deleting the variable "drupal_http_request_fails".'), 'error', FALSE);
    watchdog('integration', 'Drupal is unable to make HTTP requests. Please reset the HTTP request status.', array(), WATCHDOG_CRITICAL);
    return;
  }
  
  static $login_session_id;
  static $endpoint;
  static $api_key;
  
  // Log in to the remote system may be skipped if we already have a login 
  // session, AND the settings allow login to be skipped.
  // Skipping login causes failures for mysterious reasons: @see <http://drupal.org/node/711542>
  if (!(variable_get('content_retriever_login_once', FALSE) && isset($login_session_id))) {
    // Get static settings.
    
    $uprpc_server = variable_get('uprpc_server', '');
    $uprpc_path   = variable_get('uprpc_path', 'services/xmlrpc');

    $endpoint = $uprpc_server . $uprpc_path;
    $api_key  = variable_get('uprpc_services_api_key', '');
    
    //$endpoint = variable_get('content_retriever_endpoint', 'http://cms.defaqto.com/services/xmlrpc');
    //$api_key  = variable_get('content_retriever_api_key', '');

    // Connect to the remote system service to get an initial session id to log in with.
    $connect = xmlrpc($endpoint, 'system.connect');
    $session_id = $connect['sessid'];
    
    // We may want to call only system.connect for testing purposes.
    if ($method == 'system.connect') {
      return $connect; 
    }
  
    // Get the API key-related arguments.
    $key_args = _uprpc_import_xmlrpc_key_args('user.login');
    
    // Get account details
    $username = variable_get('uprpc_webservice_username', '');
    $password = variable_get('uprpc_webservice_pass', '');
  
    // Build the array of connection arguments we need to log in.
    $login_args = array_merge(
      array($endpoint, 'user.login'),
      $key_args,
      array($session_id),
      array($username, $password)
    );
  
    // Call the xmlrpc method with our array of arguments. This accounts for 
    // whether we use a key or not, and the extra parameters to pass to the method.
    $login = call_user_func_array('xmlrpc', $login_args);
    $login_session_id = $login['sessid'];   
     
    // We may want to call only user.login for testing purposes.
    if ($method == 'user.login') {
      return $login; 
    }
  }

  // Evil special case for node.load.
  // For WTF reasons the 'node.load' method does not use a key.
  // Rather than hacking node_service_service() (which would then require a
  // patch to Services to be bundled with this package), node.load gets a
  // special case here.
  if ($method == 'node.load') {
    $function_args  = func_get_args();
    // Slice out the ones that are arguments to the method call: everything past
    // the 1st argument.
    $method_args = array_slice($function_args, 1);

    // Build the array of connection arguments for the method we want to call.
    $xmlrpc_args = array_merge(
      array($endpoint, $method),
      array($login_session_id),
      $method_args
    );

    // Call the xmlrpc method with our array of arguments.
    $result = call_user_func_array('xmlrpc', $xmlrpc_args);

    return $result;
  }
  // ==================== end evil special

  // Get the API key-related arguments.
  $key_args = _uprpc_import_xmlrpc_key_args($method);
  
  // Get all the arguments this function has been passed.
  $function_args  = func_get_args();
  // Slice out the ones that are arguments to the method call: everything past
  // the 1st argument.
  $method_args    = array_slice($function_args, 1);

  // Build the array of connection arguments for the method we want to call.
  $xmlrpc_args = array_merge(
    array($endpoint, $method),
    $key_args,
    array($login_session_id),
    $method_args
  );

  // Call the xmlrpc method with our array of arguments.
  $result = call_user_func_array('xmlrpc', $xmlrpc_args);
  return $result;
}

/**
 * Helper function to build the xmlrpc arguments needed to use an API key.
 *
 * This should be called to build xmlrpc() arguments regardless of whether a
 * key is actually in use, as this is taken into account here.
 *
 * @param $method
 *  The name of the method we are going to call with these parameters.
 *
 * @return
 *  An array of parameters. If no key is being used, this will be empty.
 */
function _uprpc_import_xmlrpc_key_args($method) { 
  $api_key = variable_get('uprpc_services_api_key', '');
  
  // Build the API key arguments - if no key supplied supplied, presume not required
  if ($api_key != '') {
    //use api key to get a hash code for the service.
    $timestamp = (string) strtotime("now");
    $domain = $_SERVER['SERVER_NAME'];
    if ($_SERVER['SERVER_PORT'] != 80) {
      $domain .= ':' . $_SERVER['SERVER_PORT'];        
    }
    $nonce = _uprpc_import_get_unique_code('10');
    $hash_parameters = array(
      $timestamp,
      $domain,
      $nonce,
      $method,
    );
    $hash = hash_hmac("sha256", implode(';', $hash_parameters), $api_key);
    
    $key_args = array($hash, $domain, $timestamp, $nonce);
  }
  else {
    $key_args = array();
  }
  
  return $key_args;
}

/**
 * Helper function to generate the $nonce for the xmlrpc hash.
 */
function _uprpc_import_get_unique_code($length = '') {
  $code = md5(uniqid(rand(), true));
  if ($length != "") return substr($code, 0, $length);
  else return $code;	
}

/**
 * Initialize connection to the import database.
 * @TODO: obsolete.
 */
function _uprpc_init_import_database() {
  global $db_url;

  if (!is_array($db_url)) {
    $default = $db_url;
    $db_url = array(
      'default' => $db_url,
    );
  }
  
  if (!isset($db_url['import'])) {
    // TODO: Remove hardcoding and switch to variable!
    // REMEMBER: on D5 this is 'mysql', on D6 use 'mysqli'.
    $import_db_url = variable_get('uprpc_database', '');
    
    // Hardcoded until I figure out whether we can always count on the {variable}
    // table being loaded and the user having set this up when we come here!!!!
    if (!$import_db_url) {
      $import_db_url = 'mysqli://drupal:drupal@localhost/drupaldb';
    }
    
    $db_url['import'] = $import_db_url;
  }
}

/**
 * Run SQL on the old database
 *
 * @TODO: obsolete.
 */
function _uprpc_run_sql_on_other($sql) {
  static $import_db_initialized;
  
  if (!isset($import_db_initialized)) {
    _uprpc_init_import_database();
    $import_db_initialized = TRUE;
  }

  // Switch db, run query, get results, THEN switch back
  // Otherwise we don't get the data when the connection
  // is for a different db
  $db = db_set_active('import');
  $r = db_query($sql);
  $rows = array();
  if (!is_bool($r)) { 
    while($row = db_fetch_array($r)) {
      $rows[] = $row;
    }
  }
  db_set_active($db);

  return $rows;
}

/**
 * Register an ID relationship on import
 */
function uprpc_register_ids($old_id, $new_id, $type = 'node') {
  $record = array('old_id' => $old_id, 'new_id' => $new_id, 'type' => $type);
  $update_cols = array();

  if (uprpc_retrieve_by_old_id($type, $old_id)) {
    $update_cols = array('old_id', 'type');
  }
  drupal_write_record('import_ids', $record, $update_cols);
}

/**
 * Reset all import-tracking metrics
 */
function uprpc_reset_import() {
  // Todo: remove this -- it's crack!
  _uprpc_run_sql_on_other("DELETE FROM {imported} WHERE type = 'node'");
  db_query("TRUNCATE {import_ids}");
}

/**
 * Import a vocabulary from a legacy ID
 */
function uprpc_import_vocab($vid) {
  // Try to get vocab info from D5
  $rows = _uprpc_run_sql_on_other("SELECT * FROM {vocabulary} WHERE vid = " . (int) $vid);
  if (!$rows) {
    drupal_set_message(t("Could not get any information from old db for vocab @vid", array('@vid' => $vid)));
    return;
  }

  // Vocab already imported?
  $v = uprpc_retrieve_by_old_id('vocabulary', $vid);
  if (!$v) {
    $v = $rows[0];

    unset($term['tid']); // wtf is $term?
    // Tables the same, so unset the vocab ID, save, and then register
    unset($v['vid']);
    drupal_write_record('vocabulary', $v);
    uprpc_register_ids($vid, $v['vid'], 'vocabulary');
  }

  $new_vid = $v['vid'];

  // Terms!
  $tree = _uprpc_rpc_vocab($vid);

  // Import terms: record parents but don't write
  $parents = array();
  $new_to_old = array();
  $old_to_new = array();
  $new_terms = array();
  foreach ($tree as $term) {
    $new_term = uprpc_import_term($term, $new_vid);
    // Record old parent by new tid
    $parents[$new_term['tid']] = $term['parents'];
    $new_to_old[$new_term['tid']] = $term['tid'];
    $old_to_new[$term['tid']] = $new_term['tid'];
    $new_terms[$new_term['tid']] = $new_term;
  }

  // Now write parents
  foreach ($new_terms as $tid => $term) {
    if (count($parents[$tid]) == 1 && $parents[$tid][0] == 0) {
      continue;
    }
    foreach ($parents[$tid] as $parent_tid) {
      $term['parent'] = $old_to_new[$parent_tid];
    }
    // Re-save term
    taxonomy_save_term($term);
  }
}

/**
 * Import a taxonomy term into a new vocabulary
 */
function uprpc_import_term($term, $new_vid) {
  // Set old tid to one side, to establish relationships later
  $old_tid = $term['tid'];
  unset($term['tid']);
  // Can't import parents at this phase: these would all be legacy tids
  $term['parents'] = array();
  $term['vid'] = $new_vid;

  // Relationship already exists? Get tid and save term against that
  $existing_term = uprpc_retrieve_by_old_id('term', $old_tid);
  if ($existing_term) { 
    $do_not_register = TRUE;
    $term['tid'] = $existing_term['tid']; 
  }

  // Now save and register relationship
  taxonomy_save_term($term);
  $do_not_register || uprpc_register_ids($old_tid, $term['tid'], 'term');

  return $term;

}

/**
 * Saving a node, but fixing the uid on {node} and {node_revisions}
 */
function uprpc_node_save(&$node) {
  // Fix user to be the old user
  $temp_uid = $node->uid;
  node_save($node);
  $node->uid = $temp_uid;
  $node->revision_uid = $temp_uid;
  uprpc_force_uid_for_nid($node->nid, $temp_uid);
}

/**
 * Force a node<->uid
 */
function uprpc_force_uid_for_nid($nid, $uid) {
  db_query("UPDATE {node_revisions} SET uid = %d WHERE nid = %d", $uid, $nid);
  db_query("UPDATE {node} SET uid = %d WHERE nid = %d", $uid, $nid);
}

/**
 * Retrieve new object by old ID
 */
function uprpc_retrieve_by_old_id($type, $old_id, $just_id = FALSE) {
  $result = db_query("SELECT new_id FROM {import_ids} WHERE old_id = %d AND type = '%s'", $old_id, $type);
  $row = db_fetch_array($result);

  // Sense checks, and maybe just return ID
  if (!$row['new_id']) { 
    return; 
  }
  if ($just_id) { 
    return $row['new_id']; 
  }

  switch ($type) {
    case 'vocabulary':
      $result = db_query("SELECT * FROM {vocabulary} WHERE vid = %d", $row['new_id']);
      return db_fetch_array($result);
    case 'term':
      $result = db_query("SELECT * FROM {term_data} WHERE tid = %d", $row['new_id']);
      return db_fetch_array($result);
    case 'node':
      $result = node_load($row['new_id']);
      return $result;
    case 'image':
      $result = db_query("SELECT * FROM {files} WHERE fid = %d", $row['new_id']);
      return db_fetch_array($result);
    case 'user':
      $result = user_load(array('uid' => $row['new_id']));
      return $result;
  }
}

/**
 * Set a node's phase
 *
 * Nodes will be initially imported, and then they'll have various
 * post-processing functions run on them. This tracks those as numeric
 * phases
 */
function uprpc_set_phase($nid, $phase = uprpc_PHASE_NEW, $superimpose = TRUE) {
  $result = db_query("SELECT * FROM {import_phases} WHERE nid = %d", $nid);
  if ($row = db_fetch_array($result)) {
    // Phases are bitwise accumulative (like E_ALL etc.) so use bitwise arithmetic
    // to make sure all bits preserved
    $superimpose && ($phase = ($phase | $row['phase']));
    db_query("UPDATE {import_phases} SET phase = %d WHERE nid = %d", $phase, $nid);
  }
  else {

    db_query("INSERT INTO {import_phases} (phase, nid) VALUES (%d,%d)", $phase, $nid);
  }
}

/**
 * Register a redirect URL for e.g. images
 */
function uprpc_register_urls($old_url, $new_url) {
  $result = db_query("SELECT * FROM {import_urls} WHERE old_url = '%s'", $old_url);
  if ($row = db_fetch_array($result)) {
    db_query("UPDATE {import_urls} SET new_url = '%s' WHERE old_url = '%s'", $new_url, $old_url);
  }
  else {
    db_query("INSERT INTO {import_urls} (new_url,old_url) VALUES ('%s', '%s')", $new_url, $old_url);
  }
}
