<?php

/**
 * @file uprpc.batch.inc
 * Contains batch operations
 */

/**
 * Register batch and set it
 */
function uprpc_batch_go($operations = array()) {
  $batch = array(
    'title' => t('Importing'),
    'operations' => array(),
    'finished' => 'uprpc_batch_finished', // wtf -- did this function ever exist?
    'file' => drupal_get_path('module', 'uprpc') . '/uprpc.batch.inc',
  );

  // Take batch operations from the form
  foreach ($operations as $op) {
    $batch['operations'][] = array($op, array());
  }
  // Anyone else who supports batch operations, bring in their includes
  // TODO: use a file key in the hook and only load what we need
  foreach (module_implements('uprpc_batch_operation_info') as $module) {
    module_load_include("inc", $module, "$module.batch");
  }

  batch_set($batch);
}

/**
 * Batch callback for processing taxonomy terms
 * TODO: farm out to module.
 */
function uprpc_batch_terms(&$context) {
  module_load_include("inc", "uprpc");

  if(!$context['sandbox']) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['vids'] = split(",", variable_get('uprpc_vids', ''));
    $context['sandbox']['max'] = count($context['sandbox']['vids']);

    $num_done = db_query("SELECT old_id FROM {import_ids} WHERE type = 'vocabulary'");
    $context['sandbox']['vids_done'] = array();
    while($row = db_fetch_array($num_done)) {
      $context['sandbox']['vids_done'][] = $row['old_id'];
    }
  }

  $vid = array_shift($context['sandbox']['vids']);
  uprpc_import_vocab($vid);

  // Batch API progress
  $context['sandbox']['progress'] = $context['sandbox']['max'] - count($context['sandbox']['vids']);
  $context['message'] = $context['sandbox']['progress'] ." vocabularies processed out of " . $context['sandbox']['max'];
  $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
}

