
Up RPC is a system for migrating data from a Drupal 5 site to a Drupal 6 site,
for use on those site upgrades where the D6 version of the site is an entirely
new build rather than an upgrade.
Data is transmitted over XMLRPC, and the framework provides the means to
migrate users, taxonomy terms, and nodes.

Motivation
==========

Running a regular core upgrade process (ie, via update.php) is not always
a viable options. On large, complex sites, large portions of the site need
to be rebuilt, such as Views and Panels, and furthermore, the upgrade to
D6 will often call for aspects of the site to be re-made in different ways.
On community sites, new content is continually being added, and so the D6
version of the site needs to be built *before* the upgrade takes place.

This system allows you to develop a new D6 site in parallel while the old D5
site remains live and functioning. When the d6 site is ready, you need only
take the d5 site down for the time it takes to run the upgrade migration,
which in the maintainers' experience is about 1 hour per 2000 items.

Operation
=========

Both sides of the process keep track of what has been imported:

- D5 site keeps an {imported} table of ID and type, eg ('node', 5).
  This can be reset via an admin UI.
- D6 site keeps an {import_ids} table of correspondence between the old, remote
  IDs and the new IDs, eg ('node', 5, 1) indicates that node 5 on the D5 site
  has been saved as node 1 on the D6 site.

If the import process is run again, it will only import nodes which have been
created since the previous run. Also, if you reset the import records on the D5
site, the import process will import everything again, but it will compare the
IDs on what it imports, so items which have already been imported will be
updated.

Installation
============

The package includes modules for both the Drupal 5 and 6 sites, and should be
downloaded to the modules folder of both sites (if they are on the same server
you can of course just symlink).

Dependencies:
- Services 5.x-0.92

Setup instructions
==================

D5 site
-------

- install:
  - services

- enable:
  - services
  - xmlrpc server
  - system service
  - uprpc import source
  - uprpc import services
  - user service
  - user get service
  - node service (if required)
  - taxonomy service (ditto)
  
  $ drush -y en services xmlrpc_server system_service uprpc_import_services node_service taxonomy_service user_service user_get_service uprpc_import_source
  
- at admin/build/services, set up services:
  - create an API key for your D6 domain.
    - domain: The domain of the D6 importing site. Note that if you are using
    a nonstandard port, this needs to be included too. For example, on a
    localhost, your domain might be 'localhost:8888'.
  - settings:
    - use keys: TRUE
    - token expiry: 300000 (or something huge)
    - use sessid: TRUE

- create a role 'webservice'

- create a user to access services, with this role.
  
- add permissions for the anonymous role:
  - 'access services'

- add permissions for the webservice role:
  - 'access services'
  - 'load raw node data'
  - 'load raw user data'
  - 'run content count queries'
  - 'run import flagging queries'

Be aware that with these permissions, the webservice user can access data on
your nodes such as hidden fields.

TODO: unpublished nodes not imported. Is this a matter of giving admin nodes permission to the webservices role???

D6 site
-------

- enable:
  - Up RPC
  - Up RPC user, if needed
  - Up RPC node, if needed
  - Up RPC node filefield, if needed

$ drush -y en uprpc uprpc_import_node uprpc_import_user uprpc_import_node_filefield

- set up at admin/import/settings

Set up content types -- see the example client module.
